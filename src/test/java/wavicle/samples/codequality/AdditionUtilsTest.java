package wavicle.samples.codequality;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit tests
 */
public class AdditionUtilsTest {

    @Test
    public void addInts() {
        assertEquals(5, AdditionUtils.add(2, 3));
    }

    @Test
    public void addStrings() {
        assertEquals("HelloWorld", AdditionUtils.add("Hello", "World"));
    }
}
