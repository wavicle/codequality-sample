package wavicle.samples.codequality;

/**
 * Utilities to add two int's or two String's.
 *
 */
public final class AdditionUtils {

    /**
     * Private constructor.
     */
    private AdditionUtils() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Adds two numbers.
     * 
     * @param a
     *            the first string
     * @param b
     *            the second string
     * @return the result of concatenating the two strings in order
     */
    public static int add(final int a, final int b) {
        int c = a + b;
        return c;
    }

    /**
     * Adds two strings.
     * 
     * @param a
     *            the first string
     * @param b
     *            the second string
     * @return the result of concatenating the two strings in order
     */
    public static String add(final String a, final String b) {
        String c = a + b;
        return c;
    }
}
